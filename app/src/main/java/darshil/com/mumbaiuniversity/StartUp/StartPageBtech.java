package darshil.com.mumbaiuniversity.StartUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import darshil.com.mumbaiuniversity.CommonActivities.Btech.SemesterNumber;
import darshil.com.mumbaiuniversity.R;

public class StartPageBtech extends AppCompatActivity implements View.OnClickListener {

    Button btn_firstyear,btn_automobile,btn_chemical,btn_civil,btn_computer,btn_etrx,btn_extc,btn_it,btn_mechanical,btn_production;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page_engg);

        init();



    }

    private void init() {

        //initialize buttons

        btn_firstyear= findViewById(R.id.btn_firstyear);
        btn_automobile = findViewById(R.id.btn_automobile);
        btn_chemical=findViewById(R.id.btn_chemical);
        btn_civil=findViewById(R.id.btn_civil);
        btn_computer=findViewById(R.id.btn_computer);
        btn_etrx=findViewById(R.id.btn_etrx);
        btn_extc=findViewById(R.id.btn_extc);
        btn_it=findViewById(R.id.btn_it);
        btn_mechanical=findViewById(R.id.btn_mechanical);
        btn_production=findViewById(R.id.btn_production);



        //handle onclick
        btn_firstyear.setOnClickListener(this);
        btn_automobile.setOnClickListener(this);
        btn_civil.setOnClickListener(this);
        btn_chemical.setOnClickListener(this);
        btn_computer.setOnClickListener(this);
        btn_extc.setOnClickListener(this);
        btn_etrx.setOnClickListener(this);
        btn_it.setOnClickListener(this);
        btn_mechanical.setOnClickListener(this);
        btn_production.setOnClickListener(this);


/*
        //get json from network
        jsonApi = new JsonApi(JSONURL);
        String jsonString = jsonApi.getJsonApi();

        //parse json
        gson = new Gson();
        dataUtils = gson.fromJson(jsonString, DataUtils.class);
        recordsList = dataUtils.getRecords();*/


    }

    //onclick for all streams
    @Override
    public void onClick(View view) {

        Intent intent = new Intent(StartPageBtech.this, SemesterNumber.class);

        switch (view.getId())
        {

            case R.id.btn_firstyear:
            {
                //String feListSend = gson.toJson(firstyearList);
                intent.putExtra("stream", "FE");
                intent.putExtra("stream_token","10");
                startActivity(intent);
                break;
            }
            case R.id.btn_automobile:
            {

                intent.putExtra("stream", "Automobile");
                intent.putExtra("stream_token","1");
                startActivity(intent);
                break;
            }
            case R.id.btn_civil:
            {
                intent.putExtra("stream", "Civil");
                intent.putExtra("stream_token","4");
                startActivity(intent);
                break;
            }
            case R.id.btn_chemical:
            {
                intent.putExtra("stream", "Chemical");
                intent.putExtra("stream_token","8");
                startActivity(intent);
                break;
            }
            case R.id.btn_computer:
            {
                intent.putExtra("stream", "Computer");
                intent.putExtra("stream_token","2");
                startActivity(intent);
                break;
            }
            case R.id.btn_etrx:
            {
                intent.putExtra("stream", "ETRX");
                intent.putExtra("stream_token","6");
                startActivity(intent);
                break;
            }
            case R.id.btn_extc:
            {
                intent.putExtra("stream", "ETXC");
                intent.putExtra("stream_token","7");
                startActivity(intent);
                break;
            }
            case R.id.btn_it:
            {
                intent.putExtra("stream", "IT");
                intent.putExtra("stream_token","3");
                startActivity(intent);
                break;
            }
            case R.id.btn_mechanical:
            {
                intent.putExtra("stream", "Mechanical");
                intent.putExtra("stream_token","5");
                startActivity(intent);
                break;
            }
            case R.id.btn_production:
            {
                intent.putExtra("stream", "Production");
                intent.putExtra("stream_token","9");
                startActivity(intent);
                break;
            }
            default:
                Toast.makeText(this, "Developement Error.Check code once", Toast.LENGTH_SHORT).show();
        }

    }
}
