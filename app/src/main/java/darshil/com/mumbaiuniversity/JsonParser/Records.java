package darshil.com.mumbaiuniversity.JsonParser;

public class Records {

    private int Id;
    private String Subject;
    private String Semester;
    private String Stream;
    private String Month;
    private String Year;
    private String Paper;
    private String SyllabusName;
    private String SyllabusLink;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getSemester() {
        return Semester;
    }

    public void setSemester(String semester) {
        Semester = semester;
    }

    public String getStream() {
        return Stream;
    }

    public void setStream(String stream) {
        Stream = stream;
    }

    public String getPaper() {
        return Paper;
    }

    public void setPaper(String paper) {
        Paper = paper;
    }


    public String getMonth() {
        return Month;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getSyllabusName() {
        return SyllabusName;
    }

    public void setSyllabusName(String syllabusName) {
        SyllabusName = syllabusName;
    }

    public String getSyllabusLink() {
        return SyllabusLink;
    }

    public void setSyllabusLink(String syllabusLink) {
        SyllabusLink = syllabusLink;
    }
}
