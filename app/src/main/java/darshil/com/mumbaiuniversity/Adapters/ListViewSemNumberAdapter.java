package darshil.com.mumbaiuniversity.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import darshil.com.mumbaiuniversity.R;

public class ListViewSemNumberAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> semesternumber;


    TextView semesterNumber;

    public ListViewSemNumberAdapter(Context context, ArrayList semesternumber)
    {

        this.context=context;
        this.semesternumber=semesternumber;
    }

    @Override
    public int getCount() {
        return semesternumber.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
        {
            grid=inflater.inflate(R.layout.listitem_semesternumber,null);
            semesterNumber=grid.findViewById(R.id.semesterNumber);
            semesterNumber.setText(semesternumber.get(position).toString());

        }
        else
        {
            grid = (View) convertView;
        }



        return grid;
    }
}
