package darshil.com.mumbaiuniversity.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import darshil.com.mumbaiuniversity.JsonParser.Records;
import darshil.com.mumbaiuniversity.R;

public class ListViewSubjectListAdapter extends BaseAdapter {

    static int x=0;
    TextView subject_name,subject_year;
    Activity context;
    int size;
    List<Records> subjectList=new ArrayList<>();

    public ListViewSubjectListAdapter(Activity context, List<Records> subjectList, int size) {
        super();
        this.context=context;
        this.subjectList=subjectList;
        this.size=size;

    }

    private class ViewHolder {
        TextView txtViewSubjectName;
        TextView txtViewSubjectYear;
    }


    @Override
    public int getCount() {
        return size;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        ViewHolder holder;
        LayoutInflater inflater =  context.getLayoutInflater();

        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.listitem_semester_subjects, null);
            holder = new ViewHolder();
            holder.txtViewSubjectName = (TextView) convertView.findViewById(R.id.subject_name);
            holder.txtViewSubjectYear = (TextView) convertView.findViewById(R.id.subject_year);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtViewSubjectName.setText(subjectList.get(position).getSubject());
        holder.txtViewSubjectYear.setText(subjectList.get(position).getMonth()+" "+subjectList.get(position).getYear());

        return convertView;
    }

}
