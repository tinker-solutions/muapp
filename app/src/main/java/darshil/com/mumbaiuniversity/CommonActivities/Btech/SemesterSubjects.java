package darshil.com.mumbaiuniversity.CommonActivities.Btech;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import darshil.com.mumbaiuniversity.Adapters.ListViewSubjectListAdapter;
import darshil.com.mumbaiuniversity.CommonActivities.PdfView;
import darshil.com.mumbaiuniversity.JsonParser.DataUtils;
import darshil.com.mumbaiuniversity.JsonParser.Records;
import darshil.com.mumbaiuniversity.R;

public class SemesterSubjects extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    ListView listView;
    ListViewSubjectListAdapter listViewSubjectListAdapter;
    Intent intent;
    String subjectSemListString ="";
    List<Records> subjectList;
    Gson gson;
    Button btn_syllabus;
    DataUtils dataUtils;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semester_subjects);




        init();
        DownloadFilesTask myFill = new DownloadFilesTask();
        myFill.execute();
        btn_syllabus.setOnClickListener(this);




    }

    private void init() {
        listView=findViewById(R.id.listView);
        btn_syllabus=findViewById(R.id.btn_syllabus);
        subjectList=new ArrayList<>();





    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        displayPdf(subjectList.get(i).getPaper());

    }

    private void displayPdf(String URL) {

        Intent intent=new Intent(SemesterSubjects.this, PdfView.class);
        Toast.makeText(this, "Loading the paper...", Toast.LENGTH_LONG).show();
        intent.putExtra("URL",URL);
        startActivity(intent);

    }

    @Override
    public void onClick(View view) {
        if(subjectList.size()>0)
            displayPdf(subjectList.get(0).getSyllabusLink());
        else
            Toast.makeText(this, "Internship in this semester.", Toast.LENGTH_SHORT).show();
    }

    private class DownloadFilesTask extends AsyncTask<Integer, Void, String> {
        String msg="Done";

        @Override
        protected void onPreExecute() {
            intent=getIntent();
            subjectSemListString =intent.getStringExtra("subject_list");
            super.onPreExecute();
        }

        protected String doInBackground(Integer... params) {

            //convert to object
            gson = new Gson();

            dataUtils = gson.fromJson(subjectSemListString, DataUtils.class);
            subjectList = dataUtils.getRecords();



            if(subjectList.size()>0)
                btn_syllabus.setText(subjectList.get(0).getSyllabusName());
            return msg;
        }



        protected void onPostExecute(String msg) {
            listViewSubjectListAdapter=new ListViewSubjectListAdapter(SemesterSubjects.this, subjectList,subjectList.size());
            listView.setAdapter(listViewSubjectListAdapter);
            listView.setOnItemClickListener( SemesterSubjects.this);


        }
    }
}
