package darshil.com.mumbaiuniversity.CommonActivities.Btech;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import darshil.com.mumbaiuniversity.Adapters.ListViewSemNumberAdapter;
import darshil.com.mumbaiuniversity.Adapters.ListViewSubjectListAdapter;
import darshil.com.mumbaiuniversity.JsonParser.DataUtils;
import darshil.com.mumbaiuniversity.Network.RegisterAPI;
import darshil.com.mumbaiuniversity.R;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SemesterNumber extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listView;
    ListViewSemNumberAdapter listViewSemNumberAdapter;
    private ProgressBar progressBar;

    Intent intent;

    ArrayList<String> semesterlist;

    String stream_token,stream;
    String sem_token;

    //This is our root url
    public static final String ROOT_URL = "http://muapp.tinkersolutions.co.in/";
    private InterstitialAd mInterstitialAd;
    String output="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semester_number);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1450647291400144/8815576177");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        //getIntent
        intent = getIntent();
        stream = intent.getStringExtra("stream");
        stream_token=intent.getStringExtra("stream_token");


        init();
        //set adapter
        listViewSemNumberAdapter = new ListViewSemNumberAdapter(SemesterNumber.this, semesterlist);
        listView.setAdapter(listViewSemNumberAdapter);
        listView.setOnItemClickListener(this);

        /*semester1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(SemesterNumber.this,SemesterSubjects.class);
                startActivity(i);
            }
        });*/


    }

    private void init() {

        listView = findViewById(R.id.listView);

        semesterlist = new ArrayList<String>();

        //convert to object
       /* gson = new Gson();
        Type type = new TypeToken<ArrayList<Records>>() {}.getType();
        subjectlist = gson.fromJson(subjectListString, type);

        stream_token= subjectlist.get(0).getStream();
        for (int i = 0; i < subjectlist.size(); i++) {
            if (subjectlist.get(i).getSemester().equals("Semester 1")) {
                subjectlistS1.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 2")) {
                subjectlistS2.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 3")) {
                subjectlistS3.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 4")) {
                subjectlistS4.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 5")) {
                subjectlistS5.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 6")) {
                subjectlistS6.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 7")) {
                subjectlistS7.add(subjectlist.get(i));
            } else if (subjectlist.get(i).getSemester().equals("Semester 8")) {
                subjectlistS8.add(subjectlist.get(i));
            }
        }*/

        //populate with sem number
       if(stream.equals("FE"))
       {
           semesterlist.add("Semester 1");
           semesterlist.add("Semester 2");
       }
        else{
           semesterlist.add("Semester 3");
           semesterlist.add("Semester 4");
           semesterlist.add("Semester 5");
           semesterlist.add("Semester 6");
           semesterlist.add("Semester 7");
           semesterlist.add("Semester 8");
       }


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

        String subjectSemListSend="";
/*
        Toast.makeText(this, "" + subjectlist.size(), Toast.LENGTH_SHORT).show();
*/
        Intent intent = new Intent(SemesterNumber.this, SemesterSubjects.class);
        if(!stream.equals("FE"))
            i=i+2;
        if (i == 0) {
            sem_token="1";
        } else if (i == 1) {
            sem_token="2";
        } else if (i == 2) {
            sem_token="3";
        } else if (i == 3) {
            sem_token="4";
        } else if (i == 4) {
            sem_token="5";
        } else if (i == 5) {
            sem_token="6";
        } else if (i == 6) {
            sem_token="7";
        } else if (i == 7) {
            sem_token="8";
        }



        DownloadFilesTask myFill = new DownloadFilesTask();
        myFill.execute();
    }

    private void getPaper(){

        final Intent intent=new Intent(SemesterNumber.this,SemesterSubjects.class);
        //Here we will handle the http request to insert user to mysql db
        //Creating a RestAdapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //Setting the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);

        //Defining the method insertuser of our interface
        api.insertUser(

                //Passing the values by getting it from editTexts
                stream_token,
                sem_token,


                //Creating an anonymous callback
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object
                        BufferedReader reader = null;

                        //An string to store output from the server
                        output = "";

                        try {
                            //Initializing buffered reader
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        intent.putExtra("subject_list",output);
                        startActivity(intent);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        Toast.makeText(SemesterNumber.this,"Server Error Occured." ,Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    private class DownloadFilesTask extends AsyncTask<Integer, Void, String> {
        String msg="Done";

        @Override
        protected void onPreExecute() {
            progressBar = (ProgressBar) findViewById(R.id.progbar);
            progressBar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        protected String doInBackground(Integer... params) {
            getPaper();
            return msg;
        }



        protected void onPostExecute(String msg) {
            progressBar.setVisibility(View.GONE);


        }
    }
}
