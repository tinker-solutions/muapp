package darshil.com.mumbaiuniversity.quiz.models;

import java.util.ArrayList;

/**
 * Created by pritesh.patel on 2017-04-12, 3:17 PM.
 * ADESA, Canada
 */

public class QuizQuestions {
    private int questionId;
    private String question;
    private String options1;
    private String options2;
    private String options3;
    private String options4;
    private String correctAnswer;
    private String answerExplanation;
    private static ArrayList<QuizQuestions> mQuizQuestionsArrayList;

    public QuizQuestions() {
    }

    public QuizQuestions(int questionId, String question, String options1, String options2, String options3, String options4, String correctAnswer, String answerExplanation) {
        this.questionId = questionId;
        this.question = question;
        this.options1 = options1;
        this.options2 = options2;
        this.options3 = options3;
        this.options4 = options4;
        this.correctAnswer = correctAnswer;
        this.answerExplanation = answerExplanation;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptions1() {
        return options1;
    }

    public void setOptions1(String options1) {
        this.options1 = options1;
    }

    public String getOptions2() {
        return options2;
    }

    public void setOptions2(String options2) {
        this.options2 = options2;
    }

    public String getOptions3() {
        return options3;
    }

    public void setOptions3(String options3) {
        this.options3 = options3;
    }

    public String getOptions4() {
        return options4;
    }

    public void setOptions4(String options4) {
        this.options4 = options4;
    }

    public String getAnswerExplanation() {
        return answerExplanation;
    }

    public void setAnswerExplanation(String answerExplanation) {
        this.answerExplanation = answerExplanation;
    }

    public static ArrayList<QuizQuestions> getQuizQuestionsArrayList() {
        return mQuizQuestionsArrayList;
    }

    //Civil Sets
    public static void civilSet1() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Which of the following has more fire resisting characteristics?", "Marble", "Lime stone", "Compact sand stone", "Granite", "Compact sand stone", "Compact sand stone"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "The rocks which are formed due to cooling of magma at a considerable depth from earth's surface are called", "Plutonic rocks", "Hypabyssal rocks", "Volcanic rocks", "Igneous rocks", "Plutonic rocks", "Plutonic rocks"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "Plywood has the advantage of", "Greater tensile strength in longer direction", "Greater tensile strength in shorter direction", "Same tensile strength in all directions", "None of the above", "Same tensile strength in all directions", "Same tensile strength in all directions"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Due to attack of dry rot, the timber", "Cracks", "Shrinks", "Reduces to powder", "None of these", "Reduces to powder", "Reduces to powder"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Excess of alumina in brick earth makes the brick", "Impermeable", "Brittle and weak", "To lose cohesion", "To crack and warp on drying", "To crack and warp on drying", "To crack and warp on drying"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Pick up the correct statement from the following:", "In stone arches, the stones are placed with their natural beds radial", "In cornices, the stones are placed with their natural beds as vertical", "In stone walls, the stones are placed with their natural beds as horizontal", "All the above", "All the above", "All the above"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "The constituent of cement which is responsible for all the undesirable properties of cement is", "Di-calcium silicate", "Tri-calcium silicate", "Tri-calcium aluminate", "Tetra calcium alumino ferrite", "Tri-calcium aluminate", "Tri-calcium aluminate"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Inner part of a timber log surrounding the pitch, is called", "Sapwood", "Cambium layer", "Heart wood", "None to these", "Heart wood", "Heart wood"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "For testing compressive and tensile strength of cement, the cement mortar is made by mixing cement and standard sand in the proportions of", "1 : 2", "1 : 3", "1 : 4", "1 : 6", "1 : 3", "1 : 3"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "The basic purpose of a retarder in concrete is", "To increase the initial setting time of cement paste in concrete", "To decrease the initial setting time of cement paste in concrete", "To render the concrete more water tight", "To improve the workability of concrete mix", "To increase the initial setting time of cement paste in concrete", "To increase the initial setting time of cement paste in concrete"));

    }

    public static void civilSet2() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "According to I.S. : 456, slabs which span in two directions with corners held down, are assumed to be divided in each direction into middle strips and edge strips such that the width of the middle strip, is", "According to I.S. : 456, slabs which span in two directions with corners held down, are assumed to be divided in each direction into middle strips and edge strips such that the width of the middle strip, is", "Two-third of the width of the slab", "Three-fourth of the width of the slab", "Four-fifth of the width of the slab", "Three-fourth of the width of the slab", "Three-fourth of the width of the slab"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "The load stress of a section can be reduced by", "Decreasing the lever arm", "Increasing the total perimeter of bars", "Replacing larger bars by greater number of small bars", "Replacing smaller bars by greater number of greater bars", "Replacing larger bars by greater number of small bars", "Replacing larger bars by greater number of small bars"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "The diameter of the column head support a flat slab, is generally kept", "0.25 times the span length", "0.25 times the diameter of the column", "4.0 cm larger than the diameter of the column", "5.0 cm larger than the diameter of the column", "0.25 times the span length", "0.25 times the span length"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "The maximum ratio of span to depth of a slab simply supported and spanning in one direction, is", "35", "25", "30", "20", "30", "30"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "If the width of the foundation for two equal columns is restricted, the shape of the footing generally adopted, is", "Square", "Rectangular", "Trapezoidal", "Triangular", "Rectangular", "Rectangular"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "The floor slab of a building is supported on reinforced cement floor beams. The ratio of the end and intermediate spans is kept", "0.7", "0.8", "0.9", "0.6", "0.9", "0.9"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Pick up the correct statement from the following:", "Lateral reinforcement in R.C.C. columns is provided to prevent the longitudinal reinforcement from buckling", "Lateral reinforcement prevents the shearing of concrete on diagonal plane", "Lateral reinforcement stops breaking away of concrete cover, due to buckling", "All the above", "All the above", "All the above"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Pick up the incorrect statement from the following:", "Hydraulic lime is generally obtained by burning kankar", "Hydraulic lime sets slowly as compared to fat lime", "Hydraulic lime is generally used in lime mortar", "None of these", "None of these", "None of these"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Which of the following stresses is used for identifying the quality of structural steel?", "Ultimate stress", "Yield stress", "Proof stress", "None of the above", "Yield stress", "Yield stress"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "Quick lime", "Generates heat when added to water", "Reacts with carbon dioxide", "May be used for white-washing", "All the above", "All the above", "All the above"));

    }

    public static void civilSet3() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Strength of cement concrete primarily depends upon", "Quality of water", "Quantity of aggregate", "Quantity of cement", "Water-cement ratio", "Water-cement ratio", "Water-cement ratio"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "The main function of alumina in brick earth is", "To impart plasticity", "To make the brick durable", "To prevent shrinkage", "To make the brick impermeable", "To impart plasticity", "To impart plasticity"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "Cast iron", "Is obtained by purifying pig iron", "Is manufactured in required shapes", "May contain 2 to 5 per cent of carbon with other impurities", "All the above", "All the above", "All the above"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "For testing compressive strength of cement, the size of cube used is", "50 mm", "70.6 mm", "100 mm", "150 mm", "50 mm", "50 mm"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "The most commonly used retarder in cement is", "Gypsum", "Calcium chloride", "Calcium carbonate", "None of the above", "Gypsum", "Gypsum"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "To avoid large centering error with very short legs, observations are generally made", "To chain pins", "By using optical system for centering the theodolite", "To a target fixed on theodolite tripod on which theodolite may be fitted easily", "All the above", "To a target fixed on theodolite tripod on which theodolite may be fitted easily", "To a target fixed on theodolite tripod on which theodolite may be fitted easily"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Different grades are joined together by a", "Compound curve", "Transition curve", "Reverse curve", "Vertical curve", "Vertical curve", "Vertical curve"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "The line of collimation method of reduction of levels, does not provide a check on", "Intermediate sights", "Fore sights", "Back sights", "Reduced levels", "Intermediate sights", "Intermediate sights"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Which of the following methods of contouring is most suitable for a hilly terrain?", "Direct method", "Square method", "Cross-sections method", "Tachometric method", "Tachometric method", "Tachometric method"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "The chord of a curve less than peg interval, is known as", "Small chord", "Sub-chord", "Normal chord", "Short chord", "Sub-chord", "Sub-chord"));

    }

    //PhysicsSet

    public static void physicsSet1() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "The cloudy nights are warmer than clear nights because", "clouds prevent escape of radiation of heat from the ground and the air.", "absorb sunlight in the day and radiate the same in night.", "clouds make the atmosphere damp and generate heat.", "clouds obstruct the movement of air which creates heat.", "clouds prevent escape of radiation of heat from the ground and the air.", "clouds prevent escape of radiation of heat from the ground and the air."));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "An iron needle sinks in water whereas a ship made of iron floats on it because", "the edge of the needle is pointed", "the ship is flat", "the ship is driven by powerful engine", "specific gravity of the needle is greater than that of water displaced by it.", "specific gravity of the needle is greater than that of water displaced by it.", "specific gravity of the needle is greater than that of water displaced by it."));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "The leaning tower of Pisa does not fall because", "it is tappered at the top.", "it covers a large base area.", "its centre of gravity remains at the lowest position.", "the vertical line through the centre of gravity of the tower falls within the base.", "the vertical line through the centre of gravity of the tower falls within the base.", "the vertical line through the centre of gravity of the tower falls within the base."));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Pendulum clocks become slow in summer because", "days in summer are large.", "of the friction in the coil", "the length of the pendulum increases.", "the weight of the pendulum changes.", "the length of the pendulum increases.", "the length of the pendulum increases."));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Magnetic resonance imaging is based on the phenomenon of", "nuclear magnetic resonance", "electron spin resonance", "electron paramagnetic resonance", "diamagnetism of human tissues", "nuclear magnetic resonance", "nuclear magnetic resonance"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "The sun is seen before the actual sunrise because of ..... .", "reflection", "refraction", "scattering of light", "rectilinear propagation of light", "refraction", "refraction"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "What is the reason to pivot the compass needle on a sharp pin?", "To minimise the magnetic effect on the pin", "To maximize the magnetic effect on the pin", "To minimize the friction between the pin and the compass needle", "To ensure that the compass needle will not drop from the pivoted point", "To minimize the friction between the pin and the compass needle", "To minimize the friction between the pin and the compass needle"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "If a liquid is heated in space under no gravity, the transfer of heat will take place by process of", "conduction", "convection", "radiation", "cannot be heated in the absence of gravity", "radiation", "radiation"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "A ball is dropped from a satellite revolving around the earth at a height of 120 km. The ball will", "continue to move with same speed along a straight line tangentially to the satellite at that time", "continue to move with the same speed along the original orbit of satellite", "fall down to earth gradually", "go far away in space", "continue to move with the same speed along the original orbit of satellite", "continue to move with the same speed along the original orbit of satellite"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "When a sound wave goes from one medium to another, the quantity that remains unchanged is", "Frequency", "Amplitude", "Wavelength", "Speed", "Frequency", "Frequency"));

    }

    public static void physicsSet2() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Which instrument is used to measure altitudes in aircraft's ?", "Audiometer", "Ammeter", "Altimeter", "Anemometer", "Altimeter", "Altimeter"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "Which instrument is used to measure depth of ocean ?", "Galvanometer", "Fluxmeter", "Endoscope", "Fathometer", "Fathometer", "Fathometer"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "Name of the instrument to measure atomspheric pressure ?", "Barometer", "Barograph", "Bolometer", "Callipers", "Barometer", "Barometer"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Which instrument is used in submarine to see the objects above sea level ?", "Pykometer", "Polygraph", "Photometer", "Periscope", "Periscope", "Periscope"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, ". The nuclear reaction in which a heavy nucleus splits into two nuclei of nearly equal mass is called", "Nuclear fusion", "Nuclear fission", "Nuclear reaction", "Fast breeding", "Nuclear fission", "Nuclear fission"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Which unit we use to express Nuclear sizes ?", "Tesla", "Newton", "Fermi", "None of above ", "Fermi", "Fermi"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Sudden fall in barometer is indication of", "Storm", "Rain", "Tide", "Clear weather", "Storm", "Storm"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Force of attraction between the molecules of different substances is called", "Surface tension", "Cohensive force", "Adhesive force", "None of above", "Adhesive force", "Adhesive force"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "The force which opposes the relative motion between different layers of liquid or gases is called", "Critical Velocity", "Streamline Flow", "Terminal Velocity", "Viscous Force", "Viscous Force", "Viscous Force"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "What is unit of Astronomical distance ?", "light year", "angstrom ", "weber", "lux", "light year", "light year"));
    }

    public static void physicsSet3() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Radiocarbon is produced in the atmosphere as a result of ", "collision between fast neutrons and nitrogen nuclei present in the atmosphere ", "action of ultraviolet light from the sun on atmospheric oxygen ", "action of solar radiations particularly cosmic rays on carbon dioxide present in the atmosphere ", "lightning discharge in atmosphere ", "collision between fast neutrons and nitrogen nuclei present in the atmosphere ", "collision between fast neutrons and nitrogen nuclei present in the atmosphere "));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "The absorption of ink by blotting paper involves ", "viscosity of ink ", "capillary action phenomenon ", "diffusion of ink through the blotting ", "siphon action ", "capillary action phenomenon ", "capillary action phenomenon "));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "Siphon will fail to work if ", "the densities of the liquid in the two vessels are equal ", "the level of the liquid in the two vessels are at the same height ", "both its limbs are of unequal length ", "the temperature of the liquids in the two vessels are the same ", "the level of the liquid in the two vessels are at the same height ", "the level of the liquid in the two vessels are at the same height "));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Large transformers, when used for some time, become very hot and are cooled by circulating oil. The heating of the transformer is due to", "the heating effect of current alone ", "hysteresis loss alone ", "both the heating effect of current and hysteresis loss ", "intense sunlight at noon ", "both the heating effect of current and hysteresis loss ", "both the heating effect of current and hysteresis loss "));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Light year is a unit of", "time", "distance", "light ", "intensity of light ", "distance", "distance"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Mirage is due to", "unequal heating of different parts of the atmosphere", "magnetic disturbances in the atmosphere", "depletion of ozone layer in the atmosphere", "equal heating of different parts of the atmosphere", "unequal heating of different parts of the atmosphere", "unequal heating of different parts of the atmosphere"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Light from the Sun reaches us in nearly", "2 minutes", "4 minutes ", "8 minutes ", "16 minutes ", "8 minutes ", "8 minutes "));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Stars appears to move from east to west because", "all stars move from east to west ", "the earth rotates from west to east ", "the earth rotates from east to west ", "the background of the stars moves from west to east ", "the earth rotates from west to east ", "the earth rotates from west to east "));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Pa(Pascal) is the unit for ", "thrust", "pressure", "frequency", "conductivity", "pressure", "pressure"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "Let a thin capillary tube be replaced with another tube of insufficient length then, we find water", "will overflow ", "will not rise ", "depressed", "change its meniscus ", "will not rise ", "will not rise "));

    }

    //javaSet
    public static void javaSet1() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Java is developed by _____________", "Sun Microsystems of USA", "Adobe ", " Microsoft", "None", "Sun Microsystems of USA", "Sun Microsystems of USA"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "________________ is one of the inventors of Java.", "Dannis Ritchie", "James Gosling", "Bjarne Straustrup", "Balagurusamy", "James Gosling", "James Gosling"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "____________ is one of the java features that enables java program to run anywhere ", "Object-Oriented", "Dynamic & Extensible", "Platform-Independent", "Multithreaded", "Platform-Independent", "Platform-Independent"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Java compiler translates source code into _____________", "Bytecode (Virtual Machine Code)", " Bitcode", "Machine Code", "User code", "Bytecode (Virtual Machine Code)", "Bytecode (Virtual Machine Code)"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "____________ tool helps us to find errors in our programs.", "jhelp", "javah", "javap", "jdb", "jdb", "jdb"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "JVM stands for __________________", "Java Virtual Method", "Java Virtual Machine", "Java Variable & Methods", "Java Versatile Machine", "Java Virtual Machine", "Java Virtual Machine"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Which statement is use to skip the loop and continue with the next iteration?", "continue", "terminate", "skip", "break", "continue", "continue"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Range of short variable is ______________", "-128 to 128", "-128 to 127", " -32768 to 32767", "-32768 to 32768", " -32768 to 32767", " -32768 to 32767"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "We cannot create a subclass of _________ class.", "Abstract", "public", "static", "final", "final", "final"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "__________ keyword is used to inherit a class", "extend", "extends", "implement ", "implements", "extends", "extends"));

    }

    public static void javaSet2() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Which class cannot be subclassed (or extended) in java? ", "abstract class ", "parent class ", "Final class ", "None of above ", "Final class ", "Final class "));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "Why we use array as a parameter of main method", "it is syntax", "Can store multiple values", "Both of above", "None of above", "Can store multiple values", "Can store multiple values"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "Suspend thread can be revived by using", "start() method", "Suspend() method", "resume() method", "yield() method ", "resume() method", "resume() method"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Runnable is", "Class", "Method", "Variable", "Interface", "Interface", "Interface"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Which collection class associates values witch keys, and orders the keys according to their natural order", "java.util.HashSet ", "java.util.LinkedList ", "java.util.TreeMap ", "java.util.SortedSet ", "java.util.TreeMap ", "java.util.TreeMap "));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Which of the following below are valid isolation levels in J2EE", "Which of the following below are valid isolation levels in J2EE", "TRANSACTION_SERIALIZABLE", "Only A ", "Both A and B ", "Both A and B ", "Both A and B "));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Which metrhods are utilized to control the access to an object in multi threaded programming", "Asynchronized methods", "Synchronized methods ", "Serialized methods ", "None of above ", "Synchronized methods ", "Synchronized methods "));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Program which executes applet is known as", "applet engine ", "virtual machine ", "JVM", "None of above ", "applet engine ", "applet engine "));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Which statement is static and synchronized in JDBC API", "executeQuery() ", "executeUpdate() ", "getConnection() ", "prepareCall() ", "getConnection() ", "getConnection() "));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "All raw data types should be read and uploaded to the database as an array of", "int", "char", "bollean ", "byte", "byte", "byte"));

    }

    public static void javaSet3() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "After the compilation of the java source code, which file is created by the JVM ", ".class", ".java ", ".cdr ", ".doc ", ".class", ".class"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "pow () is associated with which class", "Math class", "Input stream class", "Object class", "None of above ", "Math class", "Math class"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "x=x+1 is equivalent to ", "++x ", "x++ ", "x=x-1 ", "None of these ", "x++ ", "x++ "));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "If method have same name as class name and method don’t have any return type then it is known as", "Destructors", "Object", "Variable", "Constructor", "Constructor", "Constructor"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Methods that have same name, but different Parameter list and different definition known as", "Overriding", "Constructor", "Overloading", "none of these ", "Overloading", "Overloading"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, " Inheritance means", "Sub class extends super class ", "Sub class extends Base class ", "Sub class create object of super class ", "All of the above ", "Sub class extends super class ", "Sub class extends super class "));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Process of creating exact copy of the existing object is called", "cloning", "overloading", "overriding", "coping", "cloning", "cloning"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Which is the predefined package", "Lang package ", "io package ", "util package ", "util package ", "Lang package ", "Lang package "));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Java intermediate code is known as", "First code ", "Mid code ", "Byte code ", "None of above ", "Byte code ", "Byte code "));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "Which command is used for interpretation of java program ", "Java", "javac", "javap", "none of above ", "Java", "Java"));
    }

//elecSet

    public static void elecSet1() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "A path between two or more points along which an electrical current can be carried is called a:", "network.", "relay.", "circuit.", "loop.", "circuit.", "circuit."));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "Which of the following is NOT an effect of reflective radio frequency (RF) power?", "Frequency shift", "Poor power transfer", "Radiation of noise", "Increased heat in the source", "Frequency shift", "Frequency shift"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "The frequency of the second harmonic of 60 Hz is:", "30 Hz", "60 Hz", "120 Hz", "180 Hz", "120 Hz", "120 Hz"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "The formula for electrical current is:", "Voltage / Resistance.", "Resistance * Voltage.", "Voltage + Resistance.", "Resistance / Voltage.", "Voltage / Resistance.", "Voltage / Resistance."));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Electric current is the flow of which of the following?", "Neutrons", "Photons", "Electrons", "Quarks", "Electrons", "Electrons"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Which of the following is a unit of electrical resistance?", "Volt", "Amp", "Ohm", "Coulomb", "Ohm", "Ohm"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "In a simple DC circuit with a constant voltage, where the resistance increases current will:", "decrease.", "stop.", "increase.", "remain constant.", "decrease.", "decrease."));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "Zener diodes are most commonly used in:", "voltage amplifier circuits.", "oscillator circuits.", "power supply circuits.", "current limiting circuits.", "power supply circuits.", "power supply circuits."));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Compared to bipolar transistors, field effect transistors are NOT normally characterized by:", "high input impedance.", "a reverse-biased PN junction.", "low input impedance.", "low input impedance.", "low input impedance.", "low input impedance."));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "If a gas in a fixed volume container is heated, the resultant pressure on the gas:", "remains the same.", "increases.", "decreases.", "is inversely proportional to the heat.", "increases.", "increases."));
    }

    public static void elecSet2() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "A semiconductor is formed by ……… bonds.", "Covalent", "Electrovalent", "Co-ordinate ", "None of the above ", "Covalent", "Covalent"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "A semiconductor has ………… temperature coefficient of resistance.", "Positive", "Zero", "Negative", "None of the above", "Negative", "Negative"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "A transistor has …………………", "one pn junction", "two pn junctions", "three pn junctions", "four pn junctions", "two pn junctions", "two pn junctions"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "The number of depletion layers in a transistor is …………", "four", "three", "one", "two", "two", "two"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "The base of a transistor is ………….. doped", "heavily", "moderately", "lightly", "none of the above ", "lightly", "lightly"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Addition of pentavalent impurity to a semiconductor creates many ……..", "Free electrons ", "Holes", "Valence electrons ", "Bound electrons ", "Free electrons ", "Free electrons "));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "A pentavalent impurity has ………. Valence electrons", "3", "5", "4", "6", "5", "5"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "The impurity level in an extrinsic semiconductor is about ….. of pure semiconductor.", "10 atoms for 108 atoms ", "1 atom for 108 atoms ", "1 atom for 104 atoms ", "1 atom for 100 atoms ", "1 atom for 108 atoms ", "1 atom for 108 atoms "));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "An oscillator converts ……………..", "a.c. power into d.c. power ", "d.c. power into a.c. power ", "mechanical power into a.c. power ", "none of the above ", "d.c. power into a.c. power ", "d.c. power into a.c. power "));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "In a pnp transistor, the current carriers are ………….", "acceptor ions ", "donor ions ", "free electrons ", "holes", "holes", "holes"));

    }

    public static void elecSet3() {

        mQuizQuestionsArrayList = new ArrayList<>();

        mQuizQuestionsArrayList.add(new QuizQuestions(1, "A Triac has three terminals viz ………………", "Drain, source, gate ", "Two main terminal and a gate terminal ", "Cathode, anode, gate ", "None of the above ", "Two main terminal and a gate terminal ", "Two main terminal and a gate terminal "));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "The V-I characteristics for a triac in the first and third quadrants are essentially identical to those of ………………. in its first quadrant", "Transistor", "SCR", "UJT", "none of the above ", "SCR", "SCR"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "A triac can pass a portion of …………… half-cycle through the load  ", "Only positive ", "Only negative ", "Both positive and negative ", "None of the above ", "Both positive and negative ", "Both positive and negative "));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, " A diac has  ………….. terminals", "Two", "Three", "Four", "None of the above ", "Two", "Two"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "A switch has ………………", "One state ", "Two states ", "Three states ", "None of the above ", "Two states ", "Two states "));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "A relay is ……….. switch  ", "A mechanical ", "An electronic ", "An electromechanical ", "None of the above ", "An electromechanical ", "An electromechanical "));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "he main disadvantage of a mechanical switch is that it………….  ", "Is operated mechanically ", "Is costly ", "Has high inertia ", "None of the above ", "Has high inertia ", "Has high inertia "));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "The maximum speed of electronic switch can be ………….. operations per second", "104", "10 ", "1000", "109 ", "109 ", "109 "));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "A UJT has ……………….", "Two pn junctions ", "One pn junction ", "Three pn junctions ", "None of the above ", "One pn junction ", "One pn junction "));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "A diac has ……………… semiconductor layers", "Three", "Two", "Four", "None of the above ", "Three", "Three"));

    }


    public static void mechSet1() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "Thermal conductivity of solid metals with rise in temperature normally", "increases", "decreases", "remains constant", "may increase or decrease depending on temperature", "decreases", "decreases"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "The property of a material which enables it to resist fracture due to high impact loads is known as", "elasticity", "endurance", "strength", "toughness", "toughness", "toughness"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "A hot short metal is", "brittle when cold", "brittle when hot", "brittle under all conditions", "ductile at high temperature", "brittle when hot", "brittle when hot"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "Guest’s theory of failure is applicable for following type of materials", "brittle", "ductile", "elastic", "plastic", "ductile", "ductile"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "Rankine’s theory of failure is applicable for following type of materials", "brittle", "ductile", "elastic", "plastic", "brittle", "brittle"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "Tensile strength of a mild steel specimen can be roughly predicted from following hardness test", "Brinell", "Rockwell", "Vicker", "Shore’s sceleroscope", "Brinell", "Brinell"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "Resilience of a material is important, when it is subjected to", "combined loading", "shock loading.", "thermal stresses", "wear and tear", "shock loading.", "shock loading."));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "In the case of an elastic bar fixed at upper end and loaded by a falling weight at lower end, the shock load produced can be decreased by", "decreasing the cross-section area of’ bar", "increasing the cross-section area of bar", "remain unaffected with cross-section area", "would depend upon other factors", "decreasing the cross-section area of’ bar", "decreasing the cross-section area of’ bar"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, "Other method of reducing shock load in the above case can be", "to decrease length", "to increase length", "unaffected by length", "other factors would decide same", "to increase length", "to increase length"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "The endurance limit of a material with finished surface in comparison to rough surface is", "more", "less", "same", "unpredictable.", "more", "more"));


    }

    public static void mechSet2() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1,"The unit of temperature in S.I. units is","Centigrade","Celsius","Kelvin","Rankine","Kelvin","Kelvin"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"The unit of mass in S.I. units is","kilogram","gram","tonne","quintal","kilogram","kilogram"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"The unit of energy in S.I. units is","watt","joule","weber","Amp","joule","joule"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"An ideal gas as compared to a real gas at very high pressure occupies","more volume","less volume","same volume","unpredictable behaviour","more volume","more volume"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,"General gas equation is","PV=nRT","PV=mRT","PV = C","PV=KiRT","PV=mRT","PV=mRT"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"According to Dalton’s law, the total pres sure of the mixture of gases is equal to","greater of the partial pressures of all","average of the partial pressures of all","sum of the partial pressures of all","sum of the partial pressures of all divided by average molecular weight","sum of the partial pressures of all","sum of the partial pressures of all"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"Which of the following can be regarded as gas so that gas laws could be applicable, within the commonly encountered temperature limits.","02, N2, steam, C02","Oz, N2, water vapour","S02, NH3, C02, moisture","02, N2, H2, air","02, N2, H2, air","02, N2, H2, air"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"The unit of pressure in S.I. units is","bars","mm of water column","pascal","dynes per square cm","pascal","pascal"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9,"A closed system is one in which","mass does not cross boundaries of the system, though energy may do so","mass crosses the boundary but not the energy","neither mass nor energy crosses the boundaries of the system","both energy and mass cross the boundaries of the system","mass does not cross boundaries of the system, though energy may do so","mass does not cross boundaries of the system, though energy may do so"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"Temperature of a gas is produced due to","its heating value","kinetic energy of molecules","repulsion of molecules","attraction of molecules","kinetic energy of molecules","kinetic energy of molecules"));
    }
    public static void mechSet3() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1,"The shock absorbing capacity of a bolt can be increased by","tightening it properly","making shank diameter equal to core diameter of thread.","using washer","grinding the shank","making shank diameter equal to core diameter of thread.","making shank diameter equal to core diameter of thread."));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"When a bolt is subjected to shock loading, the resilience of the bolt should be considered in order to prevent breakage at","head","shank","anywhere in the bolt.","at the thread","at the thread","at the thread"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"If an application calls for stresses on screw threads in one direction only, then the following type of thread would be best suited","buttress","square","acme","BSW","buttress","buttress"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"Relationship between the number of links (L) and number of pairs (P) is","P = 2L-4","P = 2L + 4","P = 2L+2","P = 2L-2","P = 2L+2","P = 2L+2"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,"The tendency of a body to resist change from rest or motion is known as","mass","friction","inertia","resisting force","inertia","inertia"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"The advantage of the piston valve over D-slide valve is that in the former case","wear is less","power absorbed is less","both wear and power absorbed are low","the pressure developed being high provides tight sealing","both wear and power absorbed are low","both wear and power absorbed are low"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"The Hooke’s joint consists of :","two forks","one fork","three forks","four forks","two forks","two forks"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"The e.g. of a link in any mechanism would experience","no acceleration","linear acceleration","angular acceleratio","both angular and linear accelerations","both angular and linear accelerations","both angular and linear accelerations"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9,"In elliptical trammels","three pairs turning and one pair sliding","all four pairs are turning","two pairs turning and two pairs sliding","all four pairs sliding.","two pairs turning and two pairs sliding","two pairs turning and two pairs sliding"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"The indicator using Watt mechanism is known as","Thompson indicator","Richard indicator","Simplex indicator","Thomson indicator","Richard indicator","Richard indicator"));

    }


    public static void dbmsSet1() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1, "In the relational modes, cardinality is termed as:", "Number of tuples.", "Number of attributes.", "Number of tables.", "Number of constraints.", "Number of tuples.", "Number of tuples."));
        mQuizQuestionsArrayList.add(new QuizQuestions(2, "Relational calculus is a", "Procedural language.", "Non- Procedural language.", "Data definition language.", "High level  language.", "Non- Procedural language.", "Non- Procedural language."));
        mQuizQuestionsArrayList.add(new QuizQuestions(3, "The view of total database content is ", "Conceptual view.", "Internal view.", "External view.", "Physical view.", "Conceptual view.", "Conceptual view."));
        mQuizQuestionsArrayList.add(new QuizQuestions(4, "DBMS is a collection of ………….. that enables user to create and maintain a database.", "Keys", "Translators", "Program", "Language Activity", "Program", "Program"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5, "In a relational schema, each tuple is divided into fields called ", "Relations", "Domains", "Queries", "All of the above", "Domains", "Domains"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6, "In an ER model, ……………. is described in the database by storing its data.", "Entity", "Attribute", "Relationship", "Notation", "Entity", "Entity"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7, "DFD stands for", "Data Flow Document", "Data File Diagram", "Data Flow Diagram", "Non of the above ", "Data Flow Diagram", "Data Flow Diagram"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8, "A top-to-bottom relationship among the items in a database is established by a ", "Hierarchical schema", "Network schema", "Relational Schema", "All of the above", "Hierarchical schema", "Hierarchical schema"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9, " ……………… table store information about database or about the system.", "SQL", "Nested", "System", "None of these", "System", "System"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10, "…………..defines the structure of a relation which consists of a fixed set of attribute-domain pairs.", "Instance", "Schema", "Program", "Super Key", "Schema", "Schema"));

    }

    public static void dbmsSet2() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1,"……………… clause is an additional filter that is applied to the result.","Select","Group-by","Having","Order by","Having","Having"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"A logical schema ","is the entire database","is a standard way of organizing information into accessible parts.","Describes how data is actually stored on disk.","All of the above","is a standard way of organizing information into accessible parts.","is a standard way of organizing information into accessible parts."));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"………………… is a full form of SQL.","Standard query language","Sequential query language","Structured query language","Server side query language","Structured query language","Structured query language"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"A relational database developer refers to a record as ","a criteria","a relation","a tuple","an attribute","a tuple","a tuple"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,".......... keyword is used to find the number of values in a column.","TOTAL","COUNT","ADD","SUM","COUNT","COUNT"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"An advantage of the database management approach is "," data is dependent on programs","data redundancy increases","data is integrated and can be accessed by multiple programs","none of the above","data is integrated and can be accessed by multiple programs","data is integrated and can be accessed by multiple programs"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"The collection of information stored in a database at a particular moment is called as ......","schema","instance of the database","data domain"," independence","instance of the data","instance of the data"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"A ......... is used to define overall design of the database","schema","application program","data definition language","code","schema","schema"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9,"Key to represent relationship between tables is called","primary key","secondary key","foreign key","none of the above","foreign key","foreign key"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"Grant and revoke are ....... statements.","DDL","TCL","DCL","DML","DCL","DCL"));
    }


    public static void dbmsSet3() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1," …………… database is used as template for all databases created.","Master","Model","Tempdb","None of the above","Model","Model"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"One aspect that has to be dealt with by the integrity subsystem is to ensure that only valid values can be assigned to each data items. This is referred to as","Data Security","Domain access","Data Control","Domain Integrity","Domain Integrity","Domain Integrity"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"………………….. operator is basically a join followed by a project on the attributes of first relation.","Join","Semi-Join","Full Join","Inner Join","Semi-Join","Semi-Join"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"Which of the following is not a binary operator in relational algebra?","Join","Semi-Join","Assignment","Project","Project","Project"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,"Centralizing the integrity checking directly under the DBMS ………….. Duplication and ensures the consistency and validity of the database.","Increases","Skips","Does not reduce","Reduces","Reduces","Reduces"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"Which of the following is/are the DDL statements?","Create","Drop","Alter","All of the above","All of the above","All of the above"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"In snapshot, …………………. clause tells oracle how long to wait between refreshes. ","Complete","Force","Next","Refresh","Refresh","Refresh"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"……………… defines rules regarding the values allowed in columns and is the standard mechanism for enforcing database integrity.","Column","Constraint","Index","Trigger","Constraint","Constraint"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9,"………………. First proposed the process of normalization.","Edgar. W","Edgar F. Codd"," Edward Stephen","Edward Codd","Edgar F. Codd","Edgar F. Codd"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"For using a specific database …………… command is used.","use database","database name use","Both A and B","None of them","Both A and B","Both A and B"));

    }

    public static void chemSet1() {
        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1,"Who is regarded as father of modern chemistry ? ","Ruterford","Einstein","Lavoisier","C.V. Raman","Lavoisier","Lavoisier"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"Which is not a type of elements ?","Metals"," Non Metals","Metalloids","Gases","Gases","Gases"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"Which acid is present in lemon ?","marlic acid","citric acid","lactic acid","tartaric acid","citric acid","citric acid"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"Rare gases are","mono atomic ","di atomic","tri atomic","None of above","mono atomic ","mono atomic "));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,"The term PVC used in the plastic industry stands for","polyvinyl chloride","polyvinyl carbobate","phosphor vanadiu chloride","phosphavinyl chloride","polyvinyl chloride","polyvinyl chloride"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"What among following is used to produce artificial rain ?","copper oxide","carbon monoxide","silver iodide","silver nitrate","silver iodide","silver iodide"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"Oil of vitriol is ","nitric acid","sulphuric acid","hydrochloric acid","phosphoric acid","sulphuric acid","sulphuric acid"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"Which is used in preparation of dynamite ?","glycerol","ethyl alcohol","methyl alcohol","glycol","glycerol","glycerol"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9,"What is Calcium sulphate ?","epsom salt","blue vitriol","gypsum salt","potash alum","gypsum salt","gypsum salt"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"Bleaching action of chlorine is by ","decomposition","hydrolysis","reduction","oxidation","decomposition","decomposition"));

    }
    public static void chemSet2() {

        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1," What nucleus of atom contains ?","protons","electrons","electrons and protons","protons and neutrons","protons and neutrons","protons and neutrons"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"The isotope atoms differ in ? ","number of neutrons ","atomic number ","number of electrons ","atomic weight ","number of neutrons ","number of neutrons "));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"Human bone does not contain ","calcium","carbon","oxygen","phosphorous","oxygen","oxygen"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"Homo nuclear molecules contain ?","polar bond ","covalent bond ","ionic bond ","coordinate bond ","covalent bond ","covalent bond "));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,"Air contains maximum amount of","oxygen","nitrogen","hydrogen","carbon dioxide","nitrogen","nitrogen"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"Washing soda is ?","sodium sulphite","sodium bicarbonate","sodiun carbonate","sodium biosulphite","sodiun carbonate","sodiun carbonate"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"Natural rubber is a polymer derived from ?","ethylene","propylene","isoprene","butadiene","isoprene","isoprene"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"Which of the primary component of natural gas?","Ethane","Propane","Methane","Butane","Methane","Methane"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9," The metal used to recover copper from a solution of copper sulphate is ?","Na","Ag","Hg","Fe","Fe","Fe"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"Which gas is used in fire extinguishers ?","Carbon dioxide","Nitrogen oxide","Carbon monoxide","Sulpher dioxide","Carbon dioxide","Carbon dioxide"));
    }
    public static void chemSet3() {


        mQuizQuestionsArrayList = new ArrayList<>();
        mQuizQuestionsArrayList.add(new QuizQuestions(1,"Iodine can be separated from a mixture of Iodine and Potassium Chloride by ?","Filtration","Sublimation","Distillation","Sedimentation","Sublimation","Sublimation"));
        mQuizQuestionsArrayList.add(new QuizQuestions(2,"The material which can be deformed permanently by heat and pressure is called a","thermoset","thermoplastic","chemical compound","polymer","thermoset","thermoset"));
        mQuizQuestionsArrayList.add(new QuizQuestions(3,"Mass number of a nucleus is :","the sum of the number of protons and neutrons present in the nucleus","always more than the atomic weight","always less than its atomic number","None of above","the sum of the number of protons and neutrons present in the nucleus","the sum of the number of protons and neutrons present in the nucleus"));
        mQuizQuestionsArrayList.add(new QuizQuestions(4,"Conduction band electrons have more mobility than holes because they are","experience collision more frequently","experience collision less frequently","have negative charge","need less energy to move them","experience collision less frequently","experience collision less frequently"));
        mQuizQuestionsArrayList.add(new QuizQuestions(5,"Due to rusting the weight of iron","Decreases","Increases","Remains the same","Uncertain","Increases","Increases"));
        mQuizQuestionsArrayList.add(new QuizQuestions(6,"Which gas is used to manufacture vanaspati from vegetable oil is","carbon dioxide ","nitrogen","oxygen","hydrogen","hydrogen","hydrogen"));
        mQuizQuestionsArrayList.add(new QuizQuestions(7,"The most electropositive elements among the following is :","Cs","Ca","Na","Br","Cs","Cs"));
        mQuizQuestionsArrayList.add(new QuizQuestions(8,"The Chemical Symbol of Sodium is","So","Sd","Na","Nu","Na","Na"));
        mQuizQuestionsArrayList.add(new QuizQuestions(9,"Which one is an organic Acid ? ","Sulphuric","Citric","Nitric","Phosphoric","Citric","Citric"));
        mQuizQuestionsArrayList.add(new QuizQuestions(10,"The gas used for artificial ripening of green fruit is ?","ethane","ethylene","carbon dioxide ","acetylene","ethylene","ethylene"));
    }


    }




