package darshil.com.mumbaiuniversity.quiz;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import darshil.com.mumbaiuniversity.R;
import darshil.com.mumbaiuniversity.quiz.models.QuizQuestions;


public class QuizMainActivity extends AppCompatActivity {
    Button btnNext;
    TextView txtTimer, txtQue;
    CountDownTimer mCountDownTimer;
    ArrayList<QuizQuestions> queList;
    static int currentQue = 0;
    Animation anim;
    ArrayList<Integer> questionSequence;
    RadioGroup mRadioGroup;
    RadioButton rbOption1, rbOption2, rbOption3, rbOption4;
    ProgressDialog mProgressDialog;
    ProgressBar mProgressBarQuiz;
    TextView txtQno, txtQueTitle;
    static int score;
    QuizQuestions currentQuestion;
    int randomInteger;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_main);

        initView();
        initTimer();
        setUpQuizQuestions();

        blinkText();
        getUniqueRandomNumbers();
        getNextQuestion();
        // TODO add your handling code here:
        Random random = new Random();
        randomInteger = random.nextInt(3);


    }

    @Override
    public void onBackPressed() {
        exitQuiz();

    }

    private void initTimer() {
        mCountDownTimer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                long sec = millisUntilFinished / 1000;
                txtTimer.setText(sec + " Sec");
                txtTimer.setTextColor(Color.BLACK);
                if (sec <= 5) {
                    txtTimer.setTextColor(Color.RED);
                    anim.setRepeatCount(Animation.INFINITE);
                    blinkText();
                } else {
                    anim.setRepeatCount(0);
                }
            }

            public void onFinish() {
                txtTimer.setText("Done!");
                getNextQuestion();
            }
        };
    }

    private void initView() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Wait!!! Question Loading...");
        mProgressDialog.setCancelable(false);

        mProgressBarQuiz = (ProgressBar) findViewById(R.id.progressQuiz);
        txtQno = (TextView) findViewById(R.id.txtQno);

        txtTimer = (TextView) findViewById(R.id.txtTimer);
        txtQue = (TextView) findViewById(R.id.txtQue);
        txtQueTitle = (TextView) findViewById(R.id.txtQueTitle);
        txtTimer.setText("00 Sec");

        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(onButtonClick);

        mRadioGroup = (RadioGroup) findViewById(R.id.rbGroup);
        rbOption1 = (RadioButton) findViewById(R.id.rbOption1);
        rbOption2 = (RadioButton) findViewById(R.id.rbOption2);
        rbOption3 = (RadioButton) findViewById(R.id.rbOption3);
        rbOption4 = (RadioButton) findViewById(R.id.rbOption4);

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String ans = "";
                switch (checkedId) {
                    case R.id.rbOption1:
                        ans = rbOption1.getText().toString();
                        break;
                    case R.id.rbOption2:
                        ans = rbOption2.getText().toString();
                        break;
                    case R.id.rbOption3:
                        ans = rbOption3.getText().toString();
                        break;
                    case R.id.rbOption4:
                        ans = rbOption4.getText().toString();
                        break;
                }

                if (currentQuestion.getCorrectAnswer().equalsIgnoreCase(ans)) {
                    score++;
                }
                Log.d("RES", "onCheckedChanged: " + ans + "[" + score + "]");
            }
        });
    }

    private void setUpQuizQuestions() {
        String token = getIntent().getStringExtra("token");


       // Toast.makeText(this, token, Toast.LENGTH_SHORT).show();
        switch (token) {
            case "civilkey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.civilSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.civilSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.civilSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.civilSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;

            case "physicskey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.physicsSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.physicsSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.physicsSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.physicsSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;
           case "javakey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.javaSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.javaSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.javaSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.javaSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;


            case "eleckey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.elecSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.elecSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.elecSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.elecSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;

            case "mechkey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.mechSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.mechSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.mechSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.mechSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;



            case "dbmskey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.dbmsSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.dbmsSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.dbmsSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.dbmsSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;



            case "chemicalkey":
                switch (randomInteger) {
                    case 0:
                        QuizQuestions.chemSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 1:
                        QuizQuestions.chemSet2();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    case 2:
                        QuizQuestions.chemSet3();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                    default:
                        QuizQuestions.chemSet1();
                        queList = QuizQuestions.getQuizQuestionsArrayList();
                        break;
                }
                break;


            default:
                break;
        }

    }

    private View.OnClickListener onButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnNext:
                    mCountDownTimer.cancel();
                    mProgressDialog.show();
                    Thread timerThread = new Thread() {
                        public void run() {
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mProgressDialog.hide();
                                        getNextQuestion();
                                    }
                                });
                            }
                        }
                    };
                    timerThread.start();
                    break;

                default:
                    Toast.makeText(getApplicationContext(), ((Button) v).getText().toString(), Toast.LENGTH_SHORT).show();

            }
        }
    };

    private void exitQuiz() {
        currentQue = 0;
        mCountDownTimer.cancel();

        AlertDialog.Builder b = new AlertDialog.Builder(QuizMainActivity.this)
                .setTitle("Exit").setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                        sendResult();
                    }
                })
                .setMessage("You will end the quiz!!!");
        b.create().show();
    }

    private void getNextQuestion() {
        if (currentQue < queList.size()) {
            //int c = randInt(0,4);
            int c = questionSequence.get(currentQue);
            currentQue++;
            mProgressBarQuiz.setProgress(currentQue);
            txtQno.setText(currentQue + "/" + queList.size());
            currentQuestion = queList.get(c);
            txtQueTitle.setText("[ Q : " + currentQue + " ] ");
            txtQue.setText(currentQuestion.getQuestion());

            mRadioGroup.clearCheck();

            rbOption1.setText(currentQuestion.getOptions1());
            rbOption2.setText(currentQuestion.getOptions2());
            rbOption3.setText(currentQuestion.getOptions3());
            rbOption4.setText(currentQuestion.getOptions4());
            mCountDownTimer.start();
        } else {
            mProgressDialog.setMessage("Cool!!! Preparing results....");
            mProgressDialog.show();
            Thread timerThread = new Thread() {
                public void run() {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgressDialog.hide();
                                finish();
                                sendResult();
                            }
                        });
                    }
                }
            };
            timerThread.start();
        }
    }

    private void sendResult() {
        Intent intent = new Intent(getApplicationContext(), QuizResultActivity.class);
        intent.putExtra("result", score);
        this.finish();
        currentQue=0;
        startActivity(intent);

    }

    private void blinkText() {

        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(50); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        txtTimer.setAnimation(anim);
    }

    public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public void getUniqueRandomNumbers() {
        questionSequence = new ArrayList<>();
        for (int i = 0; i < queList.size(); i++) {
            questionSequence.add(new Integer(i));
        }
        Collections.shuffle(questionSequence);
        for (int i = 0; i < queList.size(); i++) {
            System.out.println(questionSequence.get(i));
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }
}

