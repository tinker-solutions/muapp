package darshil.com.mumbaiuniversity.quiz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import darshil.com.mumbaiuniversity.R;

public class QuizInstructionActivity extends AppCompatActivity
{

    WebView mWebViewInstruction;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_instruction);


        mWebViewInstruction = (WebView)findViewById(R.id.webViewInstruction);
        mWebViewInstruction.loadUrl("file:///android_asset/quizinstruction.html");
    }


}
