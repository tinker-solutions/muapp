package darshil.com.mumbaiuniversity.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import darshil.com.mumbaiuniversity.MainActivity;
import darshil.com.mumbaiuniversity.R;

public class MainActivityQuizStart extends AppCompatActivity implements View.OnClickListener {
    Button javabtn, mathsbtn, electronicsbtn, chembtn, dbmsbtn, mechbtn, civilbtn, physicsbtn;
    String javakey = "javakey", eleckey = "eleckey", chemicalkey = "chemicalkey",
            dbmskey = "dbmskey", mechkey = "mechkey", civilkey = "civilkey", physicskey = "physicskey";
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_quiz_start);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1450647291400144/6025761305");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        javabtn = (Button) findViewById(R.id.java);
        javabtn.setOnClickListener(this);



        electronicsbtn = (Button) findViewById(R.id.electronics);
        electronicsbtn.setOnClickListener(this);

        chembtn = (Button) findViewById(R.id.chemistry);
        chembtn.setOnClickListener(this);

        dbmsbtn = (Button) findViewById(R.id.dbms);
        dbmsbtn.setOnClickListener(this);

        mechbtn = (Button) findViewById(R.id.mechanical);
        mechbtn.setOnClickListener(this);

        civilbtn = (Button) findViewById(R.id.civilengg);
        civilbtn.setOnClickListener(this);

        physicsbtn = (Button) findViewById(R.id.physics);
        physicsbtn.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(MainActivityQuizStart.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish(); //other link if this doesnt work https://stackoverflow.com/questions/2550099/how-to-kill-an-android-activity-when-leaving-it-so-that-it-cannot-be-accessed-fr
    }

    @Override
    public void onClick(View v) {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
        switch (v.getId()) {
            case R.id.java:
                Intent i = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i.putExtra("token", javakey);
                startActivity(i);
                break;
            case R.id.dbms:
                Intent i1 = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i1.putExtra("token", dbmskey);
                startActivity(i1);
                break;
            case R.id.electronics:
                Intent i3 = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i3.putExtra("token", eleckey);
                startActivity(i3);
                break;
            case R.id.mechanical:
                Intent i4 = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i4.putExtra("token", mechkey);
                startActivity(i4);
                break;
            case R.id.civilengg:
                Intent i5 = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i5.putExtra("token", civilkey);
                startActivity(i5);
                break;
            case R.id.chemistry:
                Intent i6 = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i6.putExtra("token", chemicalkey);
                startActivity(i6);
                break;
            case R.id.physics:
                Intent i7 = new Intent(MainActivityQuizStart.this, QuizDeskBoardActivity.class);
                i7.putExtra("token", physicskey);
                startActivity(i7);
                break;

            default:
                break;
        }

    }
}
