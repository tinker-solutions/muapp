package darshil.com.mumbaiuniversity.Network;

import android.os.StrictMode;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class JsonApi {

    String jsonurl;

    public JsonApi(){}

    public JsonApi(String jsonurl) {

        this.jsonurl=jsonurl;
    }

    public String getJsonApi() {

        String jsonstring="";
        try {
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT > 8)
            {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
                //your codes here

            }
            URL url = new URL(jsonurl);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            int responsecode = conn.getResponseCode();
            Scanner sc = new Scanner(url.openStream());
            while(sc.hasNext())
            {
                jsonstring+=sc.nextLine();
            }
            sc.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonstring;
    }
}
