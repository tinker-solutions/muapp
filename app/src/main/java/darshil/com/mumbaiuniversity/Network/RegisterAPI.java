package darshil.com.mumbaiuniversity.Network;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Belal on 11/5/2015.
 */
public interface RegisterAPI {
    @FormUrlEncoded
    //@POST("/admin/api/Api.php")
    @POST("/admin/api/android/Api.php")
    public void insertUser(
            @Field("stream_token") String stream,
            @Field("sem_token") String sem_token,

            Callback<Response> callback);
}
